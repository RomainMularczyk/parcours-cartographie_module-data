# Parcours Cartographie (SIG avec QGIS) - Module données

Dans ce bref module, je vous propose de nous intéresser à la matière première
nécessaire à la construction de cartes avec le logiciel QGIS. Celui-ci s'appuie
particulièrement sur la traduction de **données structurées** de diverses formes
dans l'optique d'en livrer des représentations cartographiques.

Dans ce module, nous verrons :
* Comment construire un fichier de données structurées
* Comment exploiter un fichier de données structurées déjà formé
* Comment associer plusieurs fichiers de données structurées afin de croiser
leurs informations
* Comment filtrer les données d'un fichier via QGIS
* Comment appliquer des rendus graphiques différenciés en fonction des données
* Comment discrétiser des données quantitatives continues
* Comment filtrer des données au sein d'une même colonne à l'aide d'expressions
régulières

Nous nous appuierons pour cela sur trois jeux de données différents :
* Un jeu de données sur le Tour de France, construit à partir de la fiche Wikipedia
des statistiques du Tour de France (https://fr.wikipedia.org/wiki/Statistiques_et_records_du_Tour_de_France#Bilan_par_nation)
et un fond de carte téléchargé depuis le site d'Eurostat (https://ec.europa.eu/eurostat/cache/GISCO/distribution/v2/countries/download/ref-countries-2016-01m.shp.zip)
* Un jeu de données de l'INSEE "Revenus, pauvreté et niveau de vie
en 2013" (https://www.insee.fr/fr/statistiques/fichier/2673683/BASE_TD_FILO_DEC_IRIS_2013.xls) que nous croiserons avec
le jeu de données "IRIS" de l'IGN et de l'INSEE (https://www.data.gouv.fr/fr/datasets/contours-iris/)
* Un jeu de données sur l'évolution des frontières et de la situation géo-politique durant la Seconde Guerre mondiale (http://www.stanford.edu/group/spatialhistory/Publications/HolocaustGeographies/EuropeanBorders_WWII.zip)